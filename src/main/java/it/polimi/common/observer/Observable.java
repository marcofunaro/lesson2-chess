package it.polimi.common.observer;


/**
 * Created by marcofunaro on 4/18/15.
 */
public interface Observable {
    void register(Observer obs);
}
