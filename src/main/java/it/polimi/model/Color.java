package it.polimi.model;

/**
 * Created by marcofunaro on 4/21/15.
 */
public enum Color {
    BLACK, WHITE
}
