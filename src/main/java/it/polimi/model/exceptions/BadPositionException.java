package it.polimi.model.exceptions;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class BadPositionException extends ChessException{
    public BadPositionException(String message) {
        super(message);
    }
}
