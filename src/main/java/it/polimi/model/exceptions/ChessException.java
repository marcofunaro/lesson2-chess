package it.polimi.model.exceptions;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class ChessException extends RuntimeException{
    public ChessException(String message) {
        super(message);
    }
}
