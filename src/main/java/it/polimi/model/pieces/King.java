package it.polimi.model.pieces;

import it.polimi.model.Color;
import it.polimi.model.Position;

/**
 * Created by marcofunaro on 4/21/15.
 */
public class King implements Piece {

    private final Color color;

    public King(Color color) {
        this.color = color;
    }

    public Boolean canMove(Position from, Position to) {
        return  !hasMovedMoreThanOneHorizontally(from, to) &&
                !hasMovedMoreThanOneVertically(from, to) &&
                !hasMovedMoreThanOneDiagonally(from, to);
    }

    private boolean hasMovedMoreThanOneHorizontally(Position from, Position to) {
        Integer horizontalDelta = Math.abs(from.getRow() - to.getRow());
        return horizontalDelta > 1;
    }

    private boolean hasMovedMoreThanOneVertically(Position from, Position to) {
        Integer verticalDelta = Math.abs(from.getCol() - to.getCol());
        return verticalDelta > 1;
    }

    private boolean hasMovedMoreThanOneDiagonally(Position from, Position to) {
        Integer horizontalDelta = Math.abs(from.getRow() - to.getRow());
        Integer verticalDelta = Math.abs(from.getCol() - to.getCol());
        return horizontalDelta + verticalDelta > 2;
    }

    public Color getColor() {
        return color;
    }
}
